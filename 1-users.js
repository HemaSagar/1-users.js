const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


//Q1 Find all users who are interested in playing video games.

function videoGames(users) {
    return Object.keys(users).filter((user) => {
        return users[user].interests
    }).filter((user) => {
        return users[user].interests.includes("Video Games")
    })

}

console.log(videoGames(users));

//Q2 Find all users staying in Germany.

function usersInGermany(users){
    return Object.keys(users).filter((user) =>{
        return users[user].nationality === "Germany";
    })

}

console.log("germany users- ",usersInGermany(users));

//Q4 Find all users with masters Degree.

function mastersDegreeUsers(users){
    return Object.keys(users).filter((user) => {
        return users[user].qualification === "Masters";
    })
}

console.log("Master degree users- ", mastersDegreeUsers(users));